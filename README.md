# INF226 Assignment2


## Getting started

in order to use this, following libraries is neccessary

```pip install flask flask_wtf flask_login flask-bcrypt apsw```

Use the `flask` command to start the web server:

```shell
$ cd login-server
$ flask run
```

If the `flask` command doesn't exist, you can start use `python -m flask run` instead.

```shell
$ python -m flask run
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
```

Open http://127.0.0.1:5000 in your browser and sign up!



## Problems, Code Refactoring and Technical details


The code has been refactored into three following different files:

messaging.py
db.py
app.py

where db.py contains all the commands for the sqlite3 database and messaging contains all the functions for the message purpose. 

I edited every SQL statement to parameterized queries, for example:

```
# Adds a new user to the database.
def add_user(user):
    c = conn.execute("""INSERT INTO users VALUES (?, ?)""", user)
    return
```
will get a input (username, password) and store it in a database, and the engine will properly escape the parameters to avoid SQL injections.
Source:
https://stackoverflow.com/questions/29528511/python-sqlite3-sql-injection-vulnerable-code



I also tried to make a users.py with all the user functions, but i failed to make it work with the LoginManager. As result, i placed the user functions inside the main app.py file.

changed the name for Loginform.py to forms.py, and added 

```
class RegisterForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    repeat_password = PasswordField('Repeat Password')
    submit = SubmitField('Submit')

```

In the function, in order to be able to register users.

I also added a register() function that will add a new user to the database.
The function will tell the user to confirm its password and check if the user already exists in the database. There is also added a 'registration.html' to display its own site for registration, and redirects back to 'login.html' if the registration is successfull. Else, it will just reload 'registration.html', but wont display any Error messages. 

I tried to implement Error messages such as,

"Cant login since the user doesnt exist"
"Wrong password"
"User already exists"

And more, but i failed to implement it with flask.flash and connect it to the front-end part in html.

For password security, im using Bcrypt from flask_bcrypt, which will hash the passwords. docs:
https://flask-bcrypt.readthedocs.io/en/1.0.1/

I was going to implement JSON Web Tokens (JWT), for security checks in the request_loader() function, but i failed to implement it correctly, and decided to remove its function. for now, request_loader() only checks if the passwords of a user matches. 
Optimally, it would be better to use JWT for authentication.

In messages.py im using Blueprint to connect the files between messages.py and app.py. The blueprint is imported into the app.py file and is registered in the Flask app.

I removed the pygmentize and the SQL posting, since after the reconfiguring of the SQL statements, it mostly displays '?' in the statements, ie.
```
"""INSERT INTO users VALUES (?, ?)"""
```

The website is safe against Cross-site request forgery since it holds a csrf token which is generated upon log in.

The send function is vulnerable to XSS attacks. A fix for this would be HTML Sanitization with DOMPurity or use of Safe Sinks in the JS code. 


## Design Overview

Main file,

app.py:
- index_html()
- user_loader()
- request_loader()
- login()
- register()
- logout()


All the functions related to the database and the database initialization,

db.py:
- get_user_by_id()
- get_msg_count()
- get_userdata_by_id()
- add_user()
- get_all_messages()
- get_msg_by_id()
- add_message()
- get_announcements()


All the functions related to the messaging,

messaging.py:
- get_next_id()
- get_timestamp()
- search()
- send()
- announcements()


htmls:
- index.html
- login.html
- registration.html



## Features


###### Registration

This feature will create a new personal user and add it to the servers database. When the registration is successfull you will be redirected to the login page.

###### Login

After Registration is completed, you can log in with your new user. 

###### Send message

You will be able to send a message to one or multiple users.
If you want to send message to multiple users, you must use a "," between each user, ie.

Alice, Bob, Tom

Like this, you will send the same message to Alice, Bob and Tom, where each message gets its own ID. The received users cant see that the same message is also sent to the other users.

###### Show all

This will show all your messages where you are either a sender or a receiver.

###### Search

With this feature, you can search for a specific message by its ID number.

###### Logout

This button will log out your user and redirect you to the login site.


## Threat models, potential attacks, vulnerabilities and how to prevent it.

Attacker agent against this application can be anyone of who is interested in doing XSS attack on the application. This vulnerability exists in the message function, where link or scripts could be written. Attacker agent could mainly be fraudster and scammers who aims to attempt phishing attack on users.

Phishing links or other harmful links could be posted and be displayed in disguise as legimite from the official application, but in reality the link is redirecting the user to a harmful website. 

###### Attack vector
Example:
step 1:
Create a user named Announcement

step 2:
Send this message to every user you know:
```
Congratulations! You have are the lucky winner of the websites surprise raffle! <a href="http://www.owasp.org?test=$varUnsafe">Click here</a > in order to claim the official prize of $1000!
```
or with more HTML and script experience, you could inject a popup that redirect the user to similar phishing scams. This is a threat against user confidentiality.

SOLUTION:
To solve this issue would be HTML Sanitization with DOMPurity or use of Safe Sinks in the JS code. 

###### other attacks?

Regarding Integrity, this application might have a vulnerability against man-in-the-middle attack, but i cant figure out how it would be done. But its important to point it out, and consider it. 

SOLUTION:
A public key cryptography between the communicating users is a protection against the man-in-the-middle attack.


There is an absolute minimal risk for Users password to be leaked, since the SQL statements are parameterized, and by then, they should be safe from sql injections.



###### Access Control
For now, there is no current Access Control system. Every user has the same access on the website. Except for File systems where only the owner of the server has access, every user can access their own messages, send a message, and log in or log out.

allthought, there should be an Admin instant, that can:
- Ban user for X amount of time, or permanently.
- Delete Users.
- Access to other features, such as announcements for the application.

This could, thought, also open up for more vulnerabilities if the Admin user credentials gets in the wrong hands.

## Total risk?

The total risk vulnerability is considered very high, based on the XSS vulnerability. For now, probably no attacker would care to attempt a phishing attack on this site, but if it was a popular messager website, this vulnerability could cause millions of dollars in damage in a phishing attack, where the attacker gathers user information, like in the Attack vector earlier.


